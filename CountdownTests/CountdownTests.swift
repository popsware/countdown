//
//  CountdownTests.swift
//  CountdownTests
//
//  Created by Mohab Ayman on 10/14/19.
//  Copyright © 2019 Mohab Ayman. All rights reserved.
//

import XCTest
@testable import Countdown

class CountdownTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testViewsIntersection() {
        // This is an example of a functional test case.
        let frame1 = CGRect(x: 0, y: 0, width: 100, height: 100)
        let frame2 = CGRect(x: 99, y: 99, width: 100, height: 100)
        let frame3 = CGRect(x: 100, y: 100, width: 100, height: 100)
        
        XCTAssertTrue(frame1.intersects(frame2))
        XCTAssertFalse(frame1.intersects(frame3))
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
