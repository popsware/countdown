//
//  ViewController.swift
//  Countdown
//
//  Created by Mohab Ayman on 10/17/19.
//  Copyright © 2019 Mohab Ayman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var startView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        startView.layer.cornerRadius = 25
        startView.layer.borderColor = UIColor.white.cgColor
        startView.layer.borderWidth = 3.0
        startView.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func playTapped(_ sender: Any) {
        startView.clickTransition()
        self.performSegue(withIdentifier: "startGame", sender: nil)
    }
}
