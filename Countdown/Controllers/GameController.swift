//
//  ViewController.swift
//  Countdown
//
//  Created by Mohab Ayman on 10/14/19.
//  Copyright © 2019 Mohab Ayman. All rights reserved.
//


import UIKit
import GTProgressBar

class GameController: UIViewController {
    
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var trialsLabel: UILabel!
    @IBOutlet weak var mainSrcView: UIView!
    @IBOutlet weak var mainDestView: UIView!
    
    var srcViews: [DraggableView]!
    var destViews: [DestView]!
    var word: String!
    var letters: [Character]!
    var mistakes: Int!
    
    var countdownTimer: Timer!
    var totalTime: Int!
    
    var progressBar: GTProgressBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        
        //add the progressbar
        progressBar = GTProgressBar(frame: CGRect(x: 0, y: 0, width: progressView.frame.width, height: progressView.frame.height))
        progressBar.progress = 1
        progressBar.barBorderColor = UIColor.systemBackground
        progressBar.barFillColor = UIColor.systemTeal
        progressBar.barBackgroundColor = UIColor.systemBackground
        progressBar.barBorderWidth = 1
        progressBar.barFillInset = 2
        //progressBar.labelTextColor = UIColor(red:0.35, green:0.80, blue:0.36, alpha:1.0)
        //progressBar.progressLabelInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        //progressBar.font = UIFont.boldSystemFont(ofSize: 18)
        //progressBar.labelPosition = GTProgressBarLabelPosition.right
        //progressBar.barMaxHeight = 12
        //progressBar.direction = GTProgressBarDirection.anticlockwise
        
        progressBar.displayLabel = false
        
        progressView.addSubview(progressBar)
        
        //calculate the views sizes after drawing them
        
        //mainSrcView.backgroundColor = .yellow
        //mainDestView.backgroundColor = .cyan
        
        initGame(animated: true, useSameWord: false)
    }
    
    
    
    
    // MARK:- Init Functions
    
    /// initialize all the required views and variables for a new game
    func initGame(animated: Bool, useSameWord: Bool){
        mistakes = Keys.Rules.ALLOWED_MISTAKES
        trialsLabel.text = "\(String(describing: mistakes!))"
        if(!useSameWord || word == nil){
            word = Keys.Rules.words[Int.random(in: 0 ..< Keys.Rules.words.count)]
            word = word.uppercased()
            letters = Array(word)
            for _ in 0 ..< Keys.Rules.choicesCount - word.count {
                letters.append(Helper.randomChar())
            }
            letters.shuffle()
        }
        
        mainSrcView.subviews.forEach({ $0.removeFromSuperview() }) // this gets things done
        mainDestView.subviews.forEach({ $0.removeFromSuperview() }) // this gets things done
        srcViews = []
        destViews = []
        buildViews(viewCount: letters.count, parentView: mainSrcView, isSourceViews: true, animated: animated)
        buildViews(viewCount: word.count, parentView: mainDestView, isSourceViews: false, animated: animated)
        if(animated)
        {
            showViews(views: srcViews)
            showViews(views: destViews)
        }
        startTimer()
    }
    
    /// build the source and destination views
    func buildViews(viewCount:Int, parentView: UIView, isSourceViews: Bool, animated: Bool) {
        
        print("building views...")
        
        let parentWidth = Int(parentView.frame.width)
        let viewWidth = ( parentWidth - ((viewCount + 1) * Keys.Rules.boxPAdding) ) / viewCount
        let viewHeight = Int(Double(viewWidth) * 1.5)
        
        print("parentWidth=\(parentWidth)")
        print("viewWidth=\(viewWidth)")
        print("viewHeight=\(viewHeight)")
        
        //let statusBarHeight = view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        
        //get the middle Y point in the parent
        let srcHeight = Int(parentView.frame.height)
        let pointY = (srcHeight - viewHeight) / 2
        
        // Add the Views
        for i in 0 ..< viewCount {
            /*
             let pointX = Int.random(in: 0 ..< Int(view.bounds.width) - sizeOfView)
             let pointY = Int.random(in: Int(statusBarHeight) ..< Int(view.bounds.height) - sizeOfView)
             */
            let pointX = Keys.Rules.boxPAdding + (i * viewWidth) + (i * Keys.Rules.boxPAdding)
            
            
            var newView: UIView!
            if(isSourceViews)
            {
                //source Views
                
                newView = DraggableView(frame: CGRect(x: pointX, y: pointY, width: viewWidth, height: viewHeight))
                
                (newView as! DraggableView).setLetter(letter: letters[i])
                srcViews.append(newView as! DraggableView)
                
                // add pan recognizer for dragging
                let panRecognizer = UIPanGestureRecognizer(target:self, action:#selector(detectPan))
                panRecognizer.cancelsTouchesInView = false      //for the touches end to work
                newView.gestureRecognizers = [panRecognizer]
            }
            else
            {
                //destination views
                newView = DestView(frame: CGRect(x: pointX, y: pointY, width: viewWidth, height: viewHeight))
                destViews.append(newView as! DestView)
            }
            newView.tag = i
            
            if(animated){
                newView.alpha = 0.0
            }
            parentView.addSubview(newView)
        }
    }
    
    ///animate fading in views
    func showViews(views: [UIView]){
        for (i,view) in views.enumerated() {
            UIView.animate(withDuration: 1.5, delay: Double(i) * 0.2, animations: {
                view.alpha = 1.0
            })
        }
    }
    
    
    
    // MARK:- Dragging Views
    
    var dragView: DraggableView? = nil
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first
        
        if let v = touch?.view as? DraggableView{
            print("dragging a dragview")
            
            let tag = v.tag
            print("tag is \(tag)")
            
            
            dragView = srcViews[tag]
            
            // Promote the touched view
            dragView!.superview?.bringSubviewToFront(dragView!)
            
            dragView!.transform = dragView!.transform.scaledBy(x: 0.8, y: 0.8)
            
            // Remember original location
            dragView!.lastLocation = dragView!.center
            
        }
        
        
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let v = dragView {
            
            //show that the card is hold
            v.transform = v.transform.scaledBy(x: 1.25, y: 1.25)
            
            //frame of the dragged Card
            let srcRect = mainSrcView.convert(v.frame, to: nil)
            
            // this will test whether the card touched a destination or did not been tried
            var touchedWrongDestination = false //did not touch any destination yet
            
            print("testing \(v.frame)")
            for destView in destViews
            {
                //frame of the current destination
                let destRect = mainDestView.convert(destView.frame, to: nil)
                print("with destviews \(destRect)")
                
                //checking for touch
                if (srcRect.intersects(destRect))
                {
                    print("view is dropped on a destination")
                    
                    let destChar:Character = word[word.index(word.startIndex, offsetBy: destView.tag)]
                    let srcChar:Character = letters[v.tag]
                    print("comparing \(destChar) with \(srcChar)")
                    
                    //destView.backgroundColor = v.backgroundColor
                    
                    if(srcChar == destChar)
                    {
                        print("correct choice")
                        if(!destView.locked)
                        {
                            destView.setLetter(letter: srcChar)
                            destView.lockView()
                            v.removeFromSuperview()
                            
                            checkForCompletion(timerEnded: false)
                            
                            touchedWrongDestination = false
                        }
                    }
                    else
                    {
                        print("wrong choice, but will keep checking for other potential touches")
                        touchedWrongDestination = true
                    }
                    
                    //break     //will continue to look for other touched destinations
                }
            }
            
            /*
            //checking for the maindest
            let mainDestRect = self.view.convert(mainDestView.frame, to: nil)
            print("with mainDestView \(mainDestRect)")
            if (srcRect.intersects(mainDestRect)) {
                //mainDestView.backgroundColor = v.backgroundColor
                
            }
            */
            
            if(touchedWrongDestination){
                mistakes = mistakes - 1

                trialsLabel.pulseTransition(duration: 0.5)
                trialsLabel.text = "\(String(describing: mistakes!))"
                
                if(mistakes == 0)
                {
                    failGame(reason: Keys.Messages.failed_attampts)
                }
            }
            
            //send view to its original location
            v.center = v.lastLocation
            
        }
        dragView = nil
    }
    @objc func detectPan(_ recognizer:UIPanGestureRecognizer) {
        if let v = dragView {
            let translation  = recognizer.translation(in: self.view)
            v.center = CGPoint(x: v.lastLocation.x + translation.x, y: v.lastLocation.y + translation.y)
        }
    }
    
    
    
    
    // MARK:- Game Rules
    
    func checkForCompletion(timerEnded: Bool) {
        var completedAll = true
        for destView in destViews {
            if(!destView.locked)
            {
                completedAll = false
            }
        }
        
        if(completedAll)
        {
            winGame()
        }
        else if(timerEnded){
            failGame(reason: Keys.Messages.failed_time)
        }
        
    }
    
    func winGame() {
        endGame(didWin: true, title: Keys.Messages.gamecomplete + " !!", message : Keys.Messages.wordis + " " + word!)
    }
    func failGame(reason: String) {
        endGame(didWin: false, title: Keys.Messages.gameover + " !!", message: reason)
    }
    func endGame(didWin: Bool, title: String, message: String) {
        endTimer()
        
        let alert = UIAlertController(title: title, message: message , preferredStyle: .alert)
        if(didWin){
            alert.addAction(UIAlertAction(title: Keys.Messages.startover, style: .default, handler: { (alert: UIAlertAction) in
                
                self.initGame(animated: true, useSameWord: false)
            }))
        }
        else{
            alert.addAction(UIAlertAction(title: Keys.Messages.tryagain, style: .default, handler: { (alert: UIAlertAction) in
                
                self.initGame(animated: true, useSameWord: true)
            }))
        }
        alert.addAction(UIAlertAction(title: Keys.Messages.quit, style: .destructive, handler: { (alert: UIAlertAction) in
            
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true)
        
    }
    
    @IBAction func quitPressed(_ sender: Any) {
        endTimer()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK:- Timer Functions
    
    func startTimer() {
        totalTime = Keys.Rules.ALLOWED_TIME
        progressBar.progress = CGFloat(totalTime) / CGFloat(Keys.Rules.ALLOWED_TIME)
        progressBar.barFillColor = UIColor.systemTeal
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        timerLabel.text = "\(timeFormatted(totalTime))"
        
        if totalTime != 0 {
            totalTime -= 1
            progressBar.progress = CGFloat(totalTime) / CGFloat(Keys.Rules.ALLOWED_TIME)
            if(totalTime == Int(Keys.Rules.ALLOWED_TIME / 2)){
                progressBar.barFillColor = UIColor.systemRed
            }
        } else
        {
            endTimer()
            checkForCompletion(timerEnded: true)
        }
    }
    
    func endTimer() {
        countdownTimer.invalidate()
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    
}

