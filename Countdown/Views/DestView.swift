//
//  DestView.swift
//  Countdown
//
//  Created by Mohab Ayman on 10/16/19.
//  Copyright © 2019 Mohab Ayman. All rights reserved.
//

import UIKit

class DestView: UIView {

    var locked: Bool = false
        override init(frame: CGRect) {
            super.init(frame: frame)

            /*
            //randomize view color
            let blueValue = CGFloat.random(in: 0 ..< 1)
            let greenValue = CGFloat.random(in: 0 ..< 1)
            let redValue = CGFloat.random(in: 0 ..< 1)

            self.backgroundColor = UIColor(red:redValue, green: greenValue, blue: blueValue, alpha: 1.0)
            */
            self.layer.borderColor = UIColor.systemGray.cgColor
            self.layer.borderWidth = 3.0
            self.layer.cornerRadius = 3.0
            
            self.layer.shadowColor = UIColor.systemGray.cgColor
            self.layer.shadowOpacity = 1
            self.layer.shadowOffset = .zero
            self.layer.shadowRadius = 10

            self.layer.backgroundColor = UIColor.systemBackground.cgColor
            
        }
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        func setLetter(letter: Character) {
            
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: frame.height))
            label.text = "\(letter)"
            label.textAlignment = .center
            self.addSubview(label)
        }
    func lockView(){

        locked = true
        self.layer.borderColor = UIColor.systemRed.cgColor
        self.layer.backgroundColor = UIColor.systemGray.cgColor
    }

}
