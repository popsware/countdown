//
//  Keys.swift
//  Countdown
//
//  Created by Mohab Ayman on 10/17/19.
//  Copyright © 2019 Mohab Ayman. All rights reserved.
//

import Foundation

class Keys {
    struct Messages {
        static let failed_time = "You have no time left"
        static let failed_attampts = "You are out of attempts"
        static let tryagain = "Try Again"
        static let gameover = "Game Over"
        static let gamecomplete = "Awesome"
        static let quit = "Quit"
        static let startover = "Start Over"
        static let wordis = "The word is"
        
    }
    struct Rules {
        static let boxPAdding = 10
        static let words = ["module", "cooler", "famous", "laptop", "tourist", "favorite", "beautiful"]
        static let choicesCount = 9
        static let ALLOWED_MISTAKES = 3
        static let ALLOWED_TIME = 60
        
    }
}
