//
//  Extensions.swift
//  Countdown
//
//  Created by Mohab Ayman on 10/23/19.
//  Copyright © 2019 Mohab Ayman. All rights reserved.
//

import UIKit
import QuartzCore

extension UIView {
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = .fade
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
    func infinitePulseTransition() {
        let pulseAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        pulseAnimation.duration = 1
        pulseAnimation.fromValue = 0
        pulseAnimation.toValue = 1
        pulseAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        pulseAnimation.autoreverses = true
        pulseAnimation.repeatCount = .greatestFiniteMagnitude
        layer.add(pulseAnimation, forKey: "animateOpacity")
    }
    func pulseTransition(duration: TimeInterval) {
            
            let oldTransform = transform
            transform = transform.scaledBy(x: 1.5, y: 1.5)
            setNeedsUpdateConstraints()
            UIView.animate(withDuration: duration) {
                //L self.frame.origin = newOrigin
                self.transform = oldTransform
                self.layoutIfNeeded()
            }
        }
    
    func clickTransition() {

        let oldTransform = transform
        transform = transform.scaledBy(x: 0.9, y: 0.9)
        setNeedsUpdateConstraints()
        UIView.animate(withDuration: 0.2) {
            //L self.frame.origin = newOrigin
            self.transform = oldTransform
            self.layoutIfNeeded()
        }
    }
}
