//
//  Helper.swift
//  Countdown
//
//  Created by Mohab Ayman on 10/17/19.
//  Copyright © 2019 Mohab Ayman. All rights reserved.
//

import Foundation

class Helper{
    static func randomChar() -> Character {
        let letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        let randomIndex = Int.random(in: 0 ..< letters.count)
        let stringIndex = letters.index(letters.startIndex, offsetBy: randomIndex)
      return letters[stringIndex]
    }
}
